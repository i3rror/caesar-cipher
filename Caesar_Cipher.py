#!/usr/bin/env python

"""Caesar_Cipher.py: Caesar Cipher Encoder & Decoder."""

__author__      = "Mohamed Aboushady 1141183"
__copyright__   = "Copyright 2018, GNU (general public license)"

#Caesar Cipher
print('\nCaesar Cipher encoder & decoder.\n')

#Set chars
chars = [ 
	"a",
	"b",
	"c",
	"d",
	"e",
	"f",
	"g",
	"h",
	"i",
	"j",
	"k",
	"l",
	"m",
	"n",
	"o",
	"p",
	"q",
	"r",
	"s",
	"t",
	"u",
	"v",
	"w",
	"x",
	"y",
	"z"
]

#Set dictionary worlds for the brute force mode
bruteforceDictionary = [
	"hello",
	"hi",
	"hey",
	"dear",
	"good",
	"bye",
	"after",
	"before",
	"please"
]

#Get the mode
while True:
	mode = str( input('Enter "E" for Encryption mode, "D" for Decryption mode,  "B" for Brute force mode: ') ).lower()
	
	#Check the mode
	if ( mode in ['encrypt', 'e', 'decrypt', 'd', 'brute-force', 'bruteforce', 'brute force', 'bf', 'b'] ):
		break

#Get the key
while not ( mode in ['brute-force', 'bruteforce', 'brute force', 'bf', 'b'] ):
	key = str( input('\nEnter the key from a to z: ')[0] ).lower()
	key_index = int( ord(key) - ord('a') )
	
	#Check the key, 0 is a, 26 is z
	if ( (key_index >= 0) and (key_index <= 26) ):
		print("\nKey index is: {0}\n".format(key_index))
		break

#Set final message var
message = ''

#Set mode
if ( mode in ['encrypt', 'e'] ):
	#Encryption mode
	print('Caesar Cipher encryption mode has been activated.\n')
	
	#Get the plain text
	plain = str( input('Enter the plain message: ') ).lower()
	#Encrypt each symbol
	for symbol in plain:
		#Check symbol
		if(symbol in chars):
			index = ( ( 25 - (ord("z") - ord(symbol)) ) + key_index )
			index = (index - 26) if (index > 25) else index
			message += chars[index]
		else:
			message += ' '

elif ( mode in ['decrypt', 'd'] ):
	#Decryption mode
	print('Caesar Cipher decryption mode has been activated.\n')
	
	#Get the encrypted text
	encrypted = str( input('Enter the encrypted message: ') ).lower()
	#Decrypt each symbol
	for symbol in encrypted:
		#Check symbol
		if(symbol in chars):
			index = ( ( 25 - (ord("z") - ord(symbol)) ) - key_index )
			index = (index + 26) if (index < 0) else index
			message += chars[index]
		else:
			message += ' '
	
elif ( mode in ['brute-force', 'bruteforce', 'brute force', 'bf', 'b'] ):
	#Decryption mode
	print('\nCaesar Cipher brute force mode has been activated.\n')
	
	#Get the encrypted text
	encrypted = str( input('Enter the encrypted message: ') ).lower()
	print('\n')
	
	#Loop 26 char
	for key_index in range(0, 25):
		#Decrypt each symbol
		for symbol in encrypted:
			#Check symbol
			if(symbol in chars):
				index = ( ( 25 - (ord("z") - ord(symbol)) ) - key_index )
				index = (index + 26) if (index < 0) else index
				message += chars[index]
			else:
				message += ' '
			
		print("Key is: ({0}) | Message is: {1}\n".format(key_index, message))
		
		#Check the brute force dictionary
		if any(word in message for word in bruteforceDictionary):
			break

		#Clear the x message
		message = ''
		
#Print message
if not ( mode in ['brute-force', 'bruteforce', 'brute force', 'bf', 'b'] ):
	print("\nThe message is: {0}\n".format(message))

#close
input("Press enter to close.")